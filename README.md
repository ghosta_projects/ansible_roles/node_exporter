Node exporter
=========

This role will install node exporter agent on your server.

platforms
------------

Ubuntu:     
  versions:     
    - focal (20.04)     
    - jammy (22.04)
